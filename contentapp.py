#!/usr/bin/python3

"""
 contentApp class
 Simple web application for managing content

 Copyright Jesus M. Gonzalez-Barahona, Gregorio Robles 2009-2015
 jgb, grex @ gsyc.es
 TSAI, SAT and SARO subjects (Universidad Rey Juan Carlos)
 October 2009 - March 2015
"""

import webapp


formulario = """
    <br/><br/>
    <form action="" method = "POST"> 
    <input type="text" name="name" value="">
    <input type="submit" value="Enviar">
    </form>
"""


class contentPostApp (webapp.webApp):
    """Simple web application for managing content.

    Content is stored in a dictionary, which is intialized
    with the web content."""

    # Declare and initialize content
    content = {'/': 'Root page',
               '/page': 'A page'
               }

    def parse(self, request):
        """Return the resource name (including /)"""
        metodo, recurso, _ = request.split(' ', 2)  # Trocea los dos primeros
        # espacios en blanco y guarda las dos primeras partes, el resto lo deshecha

        try:
            cuerpo = request.split('\r\n\r\n', 1)[1]  # Trocea una sola vez por \r\n\r\n y
            # guarda el final para obtener el cuerpo
        except IndexError:
            cuerpo = ""
        return metodo, recurso, cuerpo

    def process(self, resourceName):
        """Process the relevant elements of the request."""
        metodo, recurso, cuerpo = resourceName
        print('El metodo es: ' + metodo)
        print('El recurso es: ' + recurso)
        print('El cuerpo es: ' + cuerpo)

        if metodo == "POST":  # Guarda lo introducido en el formulario como nueva entrada
            recurso = '/' + cuerpo.split('=')[1]
            self.content[recurso] = cuerpo.split('=')[1]

        print()
        print(self.content)

        if recurso in self.content.keys():
            httpCode = "200 OK"
            if metodo == "POST":
                htmlBody = "<html><body>Guardado " + self.content[recurso] \
                       + " para el recurso " + recurso + formulario + "</body></html>"
            else:
                htmlBody = "<html><body>" + self.content[recurso] \
                           + formulario + "</body></html>"
        else:
            httpCode = "404 Not Found"
            htmlBody = "<html><body>Not Found" + formulario + "</body></html>"
        return (httpCode, htmlBody)


if __name__ == "__main__":
    testWebApp = contentPostApp("localhost", 1234)
